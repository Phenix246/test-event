package test.events;

import fr.phenix246.events.Event;
import fr.phenix246.events.annotations.SubscribeEvent;
import fr.phenix246.events.enumerations.EnumEventPriority;

public class EventListener
{
	@SubscribeEvent(priority = EnumEventPriority.HIGH)
	public void test2(Event ev)
	{
		System.out.println("test: " + ev.hasResult());
	}

	@SubscribeEvent
	public void test(Event ev)
	{
		System.out.println("cansel");
		ev.setCanceled(true);
	}

}
