package test;

import test.events.EventListener;
import fr.phenix246.events.Event;
import fr.phenix246.events.EventBus;

public class Main
{
	public static void main(String[] args)
	{
		EventBus bus = new EventBus();
		bus.register(new EventListener());
		System.out.println("Send events");
		bus.post(new Event());
	}
}
