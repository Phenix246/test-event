package fr.phenix246.events;

import java.lang.reflect.Method;
import java.util.HashMap;

import fr.phenix246.events.annotations.SubscribeEvent;
import fr.phenix246.events.enumerations.EnumEventPriority;
import fr.phenix246.events.interfaces.IEventExceptionHandler;

public class EventBus implements IEventExceptionHandler
{

	private HashMap<Object, String> listeners = new HashMap<>();
	private IEventExceptionHandler exceptionHandler;

	public EventBus()
	{
		exceptionHandler = this;
	}

	public void register(Object target)
	{
		if(listeners.containsKey(target) || target == null)
			return;

		listeners.put(target, target.getClass().getSimpleName());
	}

	public void unregister(Object object)
	{
		listeners.remove(object);
	}

	public boolean post(Event event)
	{
		try
		{
			Object[] list = listeners.keySet().toArray();
			for(EnumEventPriority priority : EnumEventPriority.values())
				for(Object instance : list)
					for(Method method : instance.getClass().getMethods())
					{
						if(method.isAnnotationPresent(SubscribeEvent.class))
						{
							if(method.getAnnotation(SubscribeEvent.class).priority().equals(priority))
								method.invoke(instance, event);
						}
					}
		}
		catch(Throwable throwable)
		{
			exceptionHandler.handleException(this, event, throwable);
		}
		return(event.isCancelable() ? event.isCanceled() : false);
	}

	@Override
	public void handleException(EventBus bus, Event event, Throwable throwable)
	{
		// FMLLog.log(Level.ERROR, throwable,
		// "Exception caught during firing event %s:", event);
		// FMLLog.log(Level.ERROR, "Index: %d Listeners:", index);
		// for (int x = 0; x < listeners.length; x++)
		// {
		// FMLLog.log(Level.ERROR, "%d: %s", x, listeners[x]);
		// }
	}
}
