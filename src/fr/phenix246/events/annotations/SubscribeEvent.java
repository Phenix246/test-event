package fr.phenix246.events.annotations;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import fr.phenix246.events.enumerations.EnumEventPriority;

@Retention(value = RUNTIME)
@Target(value = METHOD)
public @interface SubscribeEvent
{
	public EnumEventPriority priority() default EnumEventPriority.NORMAL;

	public boolean receiveCanceled() default false;
}
