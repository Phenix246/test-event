package fr.phenix246.events.interfaces;

import fr.phenix246.events.Event;
import fr.phenix246.events.EventBus;

public interface IEventExceptionHandler
{
	/**
	 * Fired when a EventListener throws an exception for the specified event on
	 * the event bus. After this function returns, the original Throwable will
	 * be propogated upwards.
	 *
	 * @param bus
	 *            The bus the event is being fired on
	 * @param event
	 *            The event that is being fired
	 * @param throwable
	 *            The throwable being thrown
	 */
	void handleException(EventBus bus, Event event, Throwable throwable);
}
